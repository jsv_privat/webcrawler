import requests
from bs4 import BeautifulSoup
from urllib.parse import urlparse
from lxml import etree
import pyodbc
import datetime
from urllib.parse import parse_qs


#klassenId=16505841 NETTO
#klassenId=16505840 Brutto
url = 'https://www.golf.de/publish/turnierkalender/alle-turniere?'


def get_all_tournaments():
    resultset = db_methods.db_get('', connect_to_database(), 'DISTINCT ClubID', 'tournaments', 'clubid >= 8000   order by clubid')

    for k in resultset:
        for j in range(2021, 2022):
            for m in range(7,11):
                for i in range(1,11):
                     url ="https://www.golf.de/publish/turnierkalender/alle-turniere?page={}&breite=&laenge=&lgv=&clubnameClubnr={}&begdat=01.{}.{}&enddat=01.{}.{}&begdat_mobil=&enddat_mobil=&plzOrt=&radius=&turniername=&filter_lochanzahl=&filter_geschlecht=&filter_offen=&filter_vorgwirk=1&modus=E".format(i,k[0],m,j,m+1,j)
                     print(url)
                     get_blog_content(url)

def get_url_content(url):
    return requests.get(url).text

def get_domain(url):
    parsed_uri = urlparse(url)
    return '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)

def get_blog_content(url):
    content = get_url_content(url)
    soup = BeautifulSoup(content, "html.parser")
    if soup.find('a', {'target':'haupt'}) is not None:
        clubname = soup.find('a', {'target':'haupt'}).text

    for post in soup.findAll('a', {'title':'Ergebnisse'}):
        link = post.get('href')
        tournamentlink = get_domain(url) + 'publish/'+ link
        get_url_klassen(tournamentlink, clubname)


def get_url_klassen(url, clubname):
    content = get_url_content(url)
   # print(url)
    # übergebe html an beautifulsoup parser
    soup = BeautifulSoup(content, "html.parser")
    for post in soup.findAll('a', {'class':'uk-button mb_5'}):
        if 'Teamauswertung' in soup.text:
            print('Teamauswertung')

            continue

        if 'Team' in post.text or 'Brutto über CR' in post.text:
            print('Teamwertung')
            continue

        link = post.get('href')
        print(get_domain(url) + 'publish/' + link)
        link2 = get_domain(url) + 'publish/' + link
        if "sonderwertung=1" in link2:
            print("Sonderwertung")
        else:
            get_tournament_results(link2, clubname)


def get_tournament_results(url, clubname):
    data = []
    content = get_url_content(url)
    soup = BeautifulSoup(content, "html.parser")
    table = soup.find('table', attrs={'class': 'uk-table ci-table-design uk-table-striped'})
    datestring = soup.find('p', {'class':'uk-text-center uk-text-small'}).text.split("|")
    u = urlparse(url)

    clubid = parse_qs(u.query)['clubid']
    tournamentid = parse_qs(u.query)['tournamentid']
    tournament_name = soup.find('div',attrs={"class":"headline_column"}).text.split("|")[1]
    date = datetime.datetime.strptime(datestring[1].rstrip().lstrip(), '%d.%m.%Y')
    tournament_already_read = False
    result = db_methods.db_exists('',connect_to_database(),'*','Tournaments', 'TournamentID={}'.format(tournamentid[0]))
    if not result:
        print("Tournament not in database")
        db_methods.db_execute('', connect_to_database(), "INSERT INTO TOURNAMENTS(TournamentID, ClubID, ClubName, TournamentName, TournamentDate) VALUES({},'{}','{}','{}','{}')".format(tournamentid[0], clubid[0], clubname, tournament_name, date))
    else:
        print("Tournament already exists")
        db_methods.db_execute('', connect_to_database(), "UPDATE TOURNAMENTS SET ClubName = '{}' WHERE TournamentID = {}".format(clubname, tournamentid[0]))



    date = datetime.datetime.strptime(datestring[1].rstrip().lstrip(), '%d.%m.%Y')
    if table is None:
        return

    rows = table.find_all('tr')
    for row in rows:
        cols = row.find_all('td')
        cols = [ele.text.strip() for ele in cols]
        data.append([ele for ele in cols if ele])  # Get rid of empty values
    #print(str(len(data)) + " Teilnehmer am Turnier")
    lastRank = 1
    #Ausdrucken des Turnierdata Felds

    isNetto = False
    isBrutto = False

    if 'Netto' in data[0][0]:
        isNetto = True
        print("Nettowertung")
    elif 'Brutto' in data[0][0]:
        isBrutto = True
        print("Bruttowertung")
    print(' '.join(map(str, data)))

    for i in range(len(data)):
        if len(data[i]) > 2:
            if data[i][1] == '*':
                data[i][1] = lastRank
            else:
                lastRank = data[i][1]

            if data[i][1].isdigit():
                data[i].pop(0)

           # print(data[i])

            result = db_methods.db_exists('',connect_to_database(), '*', 'Players', "Name='{}' and Clubname='{}' ".format(data[i][1], data[i][2]))

            if not result:
                print("Player not found")
                db_methods.db_execute('', connect_to_database(),
                                      "INSERT INTO Players(ClubID, Name, Clubname, Handicap) VALUES({},'{}','{}','{}' )".format(
                                      clubid[0], data[i][1],data[i][2], '54'))
            #else:
               #   print("Player already in DB " + data[i][1])
           # print(' '.join(map(str, data)))
            pointsNetto = 0
            pointsBrutto = 0
            if len(data[i]) < 6:
                continue
            if data[i][5].isdigit():
                if isNetto:
                    pointsNetto = data[i][5]
                if isBrutto:
                    pointsBrutto = data[i][5]

            placementNetto = 0
            placementBrutto = 0
            if data[i][0].isdigit():
                if isNetto:
                    placementNetto = data[i][0]
                if isBrutto:
                    placementBrutto = data[i][0]

            hcp = 54
            if data[i][3].replace(",", "").isdigit():
                hcp = data[i][3].replace(",", ".")

            if data[i][0] != '*':
                result = db_methods.db_exists('', connect_to_database(), '*', 'TournamentXPlayers',
                                              "tournamentid='{}' and playerid=(SELECT PlayerID FROM Players WHERE name = '{}' and clubname = '{}') ".format(tournamentid[0], data[i][1], data[i][2]))
                if not result:
                    db_methods.db_execute('', connect_to_database(),
                                  "INSERT INTO TournamentXPlayers(ClubID, TournamentID, PlayerID, PlacementNetto, PlacementBrutto, PointsNetto, PointsBrutto, Handicap) VALUES({}, {}, {}, {} ,{},{},{},{} )".format(
                                      clubid[0],tournamentid[0], "(SELECT PlayerID FROM Players WHERE name = '{}' and clubname = '{}')".format(data[i][1], data[i][2]), placementNetto, placementBrutto, pointsNetto , pointsBrutto , hcp))

                if result:
                    db_methods.db_execute('', connect_to_database(),
                                          "update TournamentXPlayers "
                                          "set PlacementNetto = CASE WHEN ISNULL(PlacementNetto, 0) = 0 THEN {} ELSE PlacementNetto END"
                                          ", PlacementBrutto = CASE WHEN ISNULL(PlacementBrutto, 0) = 0 THEN {} ELSE PlacementBrutto END"
                                          ", PointsNetto = CASE WHEN ISNULL(PointsNetto, 0) = 0 THEN {} ELSE PointsNetto END"
                                          ", PointsBrutto = CASE WHEN ISNULL(PointsBrutto, 0) = 0 THEN {} ELSE PointsBrutto END"
                                          " WHERE tournamentID = {} and playerid=(SELECT PlayerID FROM Players WHERE name = '{}' and clubname = '{}')".format(
                                              placementNetto, placementBrutto, pointsNetto, pointsBrutto, tournamentid[0], data[i][1], data[i][2]))


    #print(data)

def connect_to_database():
    server = 'DESKTOP-N9STSK7\SQLEXPRESS'
    database = 'golf'

    cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER='+server+';DATABASE='+database+';Trusted_Connection=yes;')
    #cursor = cnxn.cursor()
    #cursor.execute("SELECT @@version;")
    #row = cursor.fetchone()
    #while row:
    #    print(row[0])
    #    row = cursor.fetchone()
    return cnxn


def create_database(connection):
    sql = "IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='Tournaments')" \
           "CREATE TABLE dbo.Tournaments" \
           "(" \
           "TournamentID bigint NOT NULL" \
           ", ClubID int NOT NULL" \
           ", TournamentName ntext NULL" \
           ", TournamentDate date null" \
           ") "
    sqlPlayersTable = "IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='Players')" \
                      "CREATE TABLE Players" \
                      "(" \
                      "PlayerID int IDENTITY(1,1)" \
                      ", ClubID int not null" \
                      ", Name varchar(100) null" \
                      ",Handicap money null" \
                      ")"

    sqlTournamentXPlayers = "IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='TournamentXPlayers')" \
                      "CREATE TABLE TournamentXPlayers" \
                      "(" \
                      "ID int IDENTITY(1,1)" \
                      ",ClubID int not null" \
                      ",TournamentID bigint not null" \
                      ",PlayerID int not null" \
                      ",PlacementNetto int null" \
                      ", PlacementBrutto int null" \
                      ", PointsNetto int null" \
                      ", PointsBrutto int null" \
                      ", Handicap money null" \
                      ")"

    sqlAddstuff = """IF NOT EXISTS(
  SELECT TOP 1 1
  FROM INFORMATION_SCHEMA.COLUMNS
  WHERE
    [TABLE_NAME] = 'players'
    AND [COLUMN_NAME] = 'Clubname')
BEGIN
  ALTER TABLE players
    ADD Clubname varchar(100) null
END"""

    sqlAddstuff = """IF NOT EXISTS(
      SELECT TOP 1 1
      FROM INFORMATION_SCHEMA.COLUMNS
      WHERE
        [TABLE_NAME] = 'tournaments'
        AND [COLUMN_NAME] = 'Clubname')
    BEGIN
      ALTER TABLE tournaments
        ADD Clubname varchar(200) null
    END"""

    try:
        cursor = connection.cursor()
        cursor.execute(sql)
        cursor.execute(sqlPlayersTable)
        cursor.execute(sqlTournamentXPlayers)
        cursor.execute(sqlAddstuff)

        connection.commit()
        print("Database creation")
    except ValueError:
        print("Database Issue")

class db_methods():
    def db_exists(self, connection, select, table, where):
        my_cursor = connection.cursor()

        result = my_cursor.execute("select {} from {} WHERE {}".format(select, table, where))
        return len(result.fetchall())


    def db_execute(self, connection, expression):
        try:
            my_cursor = connection.cursor()
            #print(expression)
            result = my_cursor.execute(expression)

            connection.commit()
        except ValueError:
            print("Who Cares")
        return 1
    def db_get(self, connection, select, table, where):
        my_cursor = connection.cursor()

        result = my_cursor.execute("select {} from {} WHERE {}".format(select, table, where))
        return result.fetchall()

create_database(connect_to_database())


get_all_tournaments()
